Folder content description:

lib - contains 3rd-party libraries (Google gson 2.8.0)
src - source code
README.txt - this file
sample_result.txt - result which I get running the program against provided test.zip
test.zip - test zip file provided to me
Zip2Json.jar - resulting JAR-file

How to run:
1. Download Zip2Json.jar and test.zip into the same folder
2. Enter "java -jar Zip2Json.jar test.zip"
3. Inspect the "result.txt" file.