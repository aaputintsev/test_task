package com.aputintsev.converter.serialization;

import com.aputintsev.converter.objects.Node;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Custom serializer for the Node
 *
 * sample: {
 *            "name": "Bill Pay General.ctg",
 *            "children": [
 *                   {
 *                        "name": "How do I enroll for Bill Pay.ctg",
 *                        "children": []
 *                   },
 *                   {
 *                        "name": "What can I do through Bill Pay.ctg",
 *                        "children": []
 *                   },
 *                   {
 *                        "name": "What is the fee for Bill Pay.ctg",
 *                        "children": []
 *                   }
 *                ]
 *            }
 *
 * @author A.Putintsev
 */
public class NodeJsonSerializer implements JsonSerializer<Node> {
    @Override
    public JsonElement serialize(Node node, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject object = new JsonObject();
        object.addProperty("name", node.getName());
        object.add("children", jsonSerializationContext.serialize(node.getChildren()));
        return object;
    }
}
