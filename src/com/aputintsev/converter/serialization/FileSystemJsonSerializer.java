package com.aputintsev.converter.serialization;

import com.aputintsev.converter.objects.FileSystem;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Custom serializer for the FileSystem
 *
 * sample: {
 *              size: 125,
 *              folders: [{.........}]
 *          }
 *
 * @author A.Putintsev
 */
public class FileSystemJsonSerializer implements JsonSerializer<FileSystem> {
    @Override
    public JsonElement serialize(FileSystem fileSystem, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject object = new JsonObject();
        object.addProperty("size", fileSystem.getSize());
        object.add("folders", jsonSerializationContext.serialize(fileSystem.getRoot().getChildren()));
        return object;
    }
}
