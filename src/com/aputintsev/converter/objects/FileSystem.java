package com.aputintsev.converter.objects;

import com.aputintsev.converter.objects.parsers.FileSystemParser;

/**
 * Common interface for FileSystem representation
 *
 * @author A.Putintsev
 */
public interface FileSystem {
    void setParser(FileSystemParser parser);
    void addNode(String path);

    Node getRoot();
    int getSize();

    String serializeToString();
}
