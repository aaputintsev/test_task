package com.aputintsev.converter.objects.parsers.impl;

import com.aputintsev.converter.objects.Node;
import com.aputintsev.converter.objects.TreeNode;
import com.aputintsev.converter.objects.parsers.FileSystemParser;

import java.util.ArrayDeque;
import java.util.Arrays;

/**
 * This parser incapsulates logic of parsing zip file in accordance with task.
 * It reads and stores only directories with names containing ".ctg" suffix.
 * Also it provides a method to calculate such entries.
 *
 * @author A.Putintsev
 */
public class CtgParser implements FileSystemParser {

    /**
     * Creates a folders subtree based on path. This subtree will always end with .ctg-folder, if there are any
     * non-".ctg" children, they will be omitted. Non-".ctg" parents are kept to provide tree consistency. Resulting
     * subtrees will look like these:
     *
     * path: dir1/dir2.ctg/dir3/file.txt
     * subtree: dir1
     *            |_dir2.ctg
     *
     *  path: dir1/dir2.ctg/dir3/dir4.ctg/
     *  subtree: dir1
     *            |_dir2.ctg
     *                  |_dir3
     *                      |_dir4.ctg
     *
     *  path: dir1/dir2/dir3/file.bin
     *  subtree: null
     *
     * @param path path to zip entry
     * @return TreeNode pointer to the root of subtree or null
     */
    @Override
    public TreeNode parsePath(String path) {
        if (!path.contains(".ctg/")) return null; // if no .ctg/ - just return

        boolean isDirectory = path.endsWith("/");

        //fallback if there is a path starting from "/". In this case empty string appears in array on the first place.
        String[] subpath = path.split("/");
        if (subpath[0] == null || subpath[0].isEmpty())
            subpath = Arrays.copyOfRange(subpath, 1, subpath.length);

        int maxDeep = 0;
        //count maximum deep of the ".ctg"-entry in the path
        for (int i = 0; i < subpath.length; i++) {
            if (i < subpath.length-1) {
                if (subpath[i].endsWith(".ctg")) maxDeep = i;
            } else {
                if (subpath[i].endsWith(".ctg") && isDirectory) maxDeep = i;
            }
        }

        if (maxDeep > 0) {
            //analyze folders only until there could be a ".ctg" entry
            //the end of the path is omitted
            String pathToNode = "";
            String name = subpath[0];
            boolean dir = subpath.length > 1;
            TreeNode subtree = new TreeNode(name, pathToNode, dir);
            TreeNode parent = subtree;

            for (int i = 1; i <= maxDeep; i++) {
                name = subpath[i];
                pathToNode = pathToNode + "/" + subpath[i-1];
                if (i < subpath.length - 1)
                    dir = true;
                else
                    dir = isDirectory;
                TreeNode node = new TreeNode(name, pathToNode, dir);
                parent.getChildren().add(node);
                parent = node;
            }

            return subtree;
        }

        return null;
    }

    /**
     * Calculates amount of ".ctg"-folders in the folders tree.
     * Implemented using breadth-first search algorithm.
     *
     * @param root of the folders tree
     * @return int amount of ".ctg" folders
     */
    @Override
    public int calculateSize(Node root) {
        ArrayDeque<Node> nodesQueue = new ArrayDeque<>();
        int size = 0;

        for (Node node : root.getChildren()) {
            nodesQueue.push(node);
        }
        while(!nodesQueue.isEmpty()) {
            Node node = nodesQueue.pop();
            for (Node child : node.getChildren()) {
                nodesQueue.push(child);
            }
            if (node.getName().contains(".ctg")
                    && node.isDirectory())
                size++;
        }
        return size;
    }
}
