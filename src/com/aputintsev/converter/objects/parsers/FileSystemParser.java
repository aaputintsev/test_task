package com.aputintsev.converter.objects.parsers;

import com.aputintsev.converter.objects.Node;

/**
 * Common interfaces for the classes incapsulating file parsing logic
 *
 * @author A.Putintsev
 */
public interface FileSystemParser {

    Node parsePath(String path);
    int calculateSize(Node root);

}
