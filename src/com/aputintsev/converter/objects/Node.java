package com.aputintsev.converter.objects;

import java.util.List;

/**
 * Common interface for the File System Node - folder or file
 *
 * @author A.Putintsev
 */
public interface Node {
    String getName();
    boolean isDirectory();
    List<Node> getChildren();
}
