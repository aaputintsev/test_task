package com.aputintsev.converter.objects;

import com.aputintsev.converter.objects.parsers.FileSystemParser;
import com.aputintsev.converter.serialization.FileSystemJsonSerializer;
import com.aputintsev.converter.serialization.NodeJsonSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Concrete implementation of the folders tree
 *
 * @author A.Putintsev
 */
public class TreeFileSystem implements FileSystem {

    private Node root;
    private FileSystemParser parser;

    public TreeFileSystem(String root, FileSystemParser parser) {
        this.root = new TreeNode(root, root, false);
        this.parser = parser;
    }

    public void setParser(FileSystemParser parser) {
        this.parser = parser;
    }

    public void addNode(String path) {
        if (path == null)
            throw new NullPointerException("Null path specified");

        mergeBranch(parser.parsePath(path));
    }

    public Node getRoot() {
        return this.root;
    }

    public int getSize() {
        return parser.calculateSize(this.root);
    }

    public String serializeToString() {
        Gson builder = new GsonBuilder()
                .registerTypeAdapter(TreeFileSystem.class, new FileSystemJsonSerializer())
                .registerTypeAdapter(TreeNode.class, new NodeJsonSerializer())
                .setPrettyPrinting()
                .create();

        return builder.toJson(this);
    }

    private void mergeBranch(Node branch) {
        if (branch == null) return;

        Node currentBranchLevel = branch;
        Node currentTreeLevel = root;
        while (currentBranchLevel != null) {
            if (currentTreeLevel.getChildren().isEmpty()) {
                currentTreeLevel.getChildren().add(currentBranchLevel);
                break;
            }

            int index = currentTreeLevel.getChildren().indexOf(currentBranchLevel);
            if (index == -1) {
                currentTreeLevel.getChildren().add(currentBranchLevel);
                break;
            } else {
                currentTreeLevel = currentTreeLevel.getChildren().get(index);
                if (!currentBranchLevel.getChildren().isEmpty()) {
                    currentBranchLevel = currentBranchLevel.getChildren().get(0);
                } else {
                    currentBranchLevel = null;
                }
            }
        }
    }
}
