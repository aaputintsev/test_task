package com.aputintsev.converter.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Node implementation for the folders tree. Each node can be either folder or file.
 *
 * @author A.Putintsev
 */
public class TreeNode implements Node {
    private String name;
    private String path;
    private boolean isDirectory;
    private List<Node> children;

    public TreeNode(String name, String path, boolean isDirectory) {
        this.name = name;
        this.path = path;
        this.isDirectory = isDirectory;
        this.children = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public List<Node> getChildren() {
        return children;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public boolean equals(Object that) {
        if (!(that instanceof TreeNode)) return false;

        if (this == that) return true;
        return this.name.equals(((TreeNode) that).name)
                && this.path.equals(((TreeNode) that).path)
                && this.isDirectory == ((TreeNode) that).isDirectory;
    }
}
