package com.aputintsev.converter;

import com.aputintsev.converter.conversion.FileConverter;
import com.aputintsev.converter.objects.FileSystem;
import com.aputintsev.converter.output.OutputProvider;
import com.aputintsev.converter.output.impl.TextFileOutput;
import com.aputintsev.converter.conversion.impl.ZipConversionStrategy;

/**
 * This class is a main entry point of the application.
 * The application gets zip-file, reads its content, takes only folders with .ctg suffix,
 * counts them and provides a folders tree in JSON format printed to a file.
 *
 * To run the program please put your test file to the folder with jar and use the following command:
 *  java -jar Zip2Json.jar $filename.zip$
 *
 *  i.e.: java -jar Zip2Json.jar test.zip
 *
 *  Result: file result.txt containing json structure like
 *  {
 *    "size": 125,
 *    "folders": [
 *          {
 *              "name": "category-sre",
 *              "children": [
 *                  {
 *                      "name": "FreedomBank.ctg",
 *                      "children": [
 *                          {
 *                              "name": "FreedomBank Online.ctg",
 *                              "children": [
 *                                  {
 *                                      "name": "Bill Pay eBills.ctg",
 *                                      "children": [
 *                                          {
 *                                              "name": "eBills.ctg",
 *                                              "children": []
 *                                          },
 *                   <.......>
 *  }
 *
 * @author A.Putintsev
 */
public class Converter {
    public static void main(String[] args) {
        if (args.length == 0)
            throw new IllegalArgumentException("File name can not be empty!");

        String fileName = args[0];
        FileConverter fileConverter;

        //determine file extension to choose the proper converter
        String[] nameParts = fileName.split("\\.");
        String ext = nameParts[nameParts.length - 1].toLowerCase();

        // other formats support can be added later
        switch (ext) {
            case "zip":
                fileConverter = new FileConverter(new ZipConversionStrategy());
                break;
            default:
                throw new UnsupportedOperationException("Format is not supported!");
        }

        FileSystem fileSystem = fileConverter.convertFile(fileName);
        OutputProvider outputProvider = new TextFileOutput();
        outputProvider.write(fileSystem.serializeToString());
    }
}
