package com.aputintsev.converter.output.impl;

import com.aputintsev.converter.objects.FileSystem;
import com.aputintsev.converter.objects.TreeFileSystem;
import com.aputintsev.converter.objects.TreeNode;
import com.aputintsev.converter.output.OutputProvider;
import com.aputintsev.converter.serialization.FileSystemJsonSerializer;
import com.aputintsev.converter.serialization.NodeJsonSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Implements logic for writing a text file.
 *
 * @author A.Putintsev
 */
public class TextFileOutput implements OutputProvider {

    public TextFileOutput() {
    }

    /**
     * Writes some text to the file "result.txt"
     */
    @Override
    public void write(String text) {
        writeToFile("result.txt", text);
    }

    private void writeToFile(String name, String text) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(name))) {
            out.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
