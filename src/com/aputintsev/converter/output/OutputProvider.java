package com.aputintsev.converter.output;

/**
 * Common interface representing some output mechanism
 *
 * @author A.Putintsev
 */
public interface OutputProvider {
    void write(String text);
}
