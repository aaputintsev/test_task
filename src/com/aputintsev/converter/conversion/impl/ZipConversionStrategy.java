package com.aputintsev.converter.conversion.impl;

import com.aputintsev.converter.objects.FileSystem;
import com.aputintsev.converter.objects.TreeFileSystem;
import com.aputintsev.converter.objects.parsers.impl.CtgParser;
import com.aputintsev.converter.conversion.ConversionStrategy;

import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * This class implements logic for zip file parsing
 *
 * @author A.Putintsev
 */
public class ZipConversionStrategy implements ConversionStrategy {

    /**
     * Return FileSystem instance, initialized with custom parser which incapsulates the logic of file reading.
     *
     * @param fileName of the file to parse
     * @return FileSystem instance
     */
    @Override
    public FileSystem convert(String fileName) {
        FileSystem fileSystem = new TreeFileSystem(fileName, new CtgParser());
        try {
            try (ZipFile zipFile = new ZipFile(fileName)) {
                for (Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements();) {
                    ZipEntry entry = e.nextElement();
                    fileSystem.addNode(entry.getName());
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return fileSystem;
    }
}
