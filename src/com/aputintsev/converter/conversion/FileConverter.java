package com.aputintsev.converter.conversion;

import com.aputintsev.converter.objects.FileSystem;

/**
 * Context for the file conversion from Zip (or any other format) to FileSystem representation
 *
 * @author A.Putintsev
 */
public class FileConverter {

    private ConversionStrategy conversionStrategy;

    public FileConverter(ConversionStrategy strategy) {

        this.conversionStrategy = strategy;
    }

    public FileSystem convertFile(String fileName) {

        return conversionStrategy.convert(fileName);
    }
}
