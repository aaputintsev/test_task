package com.aputintsev.converter.conversion;

import com.aputintsev.converter.objects.FileSystem;

/**
 * Common interface for conversion strategies
 *
 * @author A.Putintsev
 */
public interface ConversionStrategy {

    FileSystem convert(String fileName);

}